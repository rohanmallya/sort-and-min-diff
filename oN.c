#include<stdio.h>
#include<stdlib.h>
#include<limits.h>

int* sorting(int[],int);
void print(int[],int);
int main(){
	int size,i;
	scanf("%d",&size);
	int array[size];
	for(i=0; i<size; i++){
		scanf("%d",&array[i]);
	}
	int *a = sorting(array,size);
	printf("Smallest Difference Pairs is %d, %d\n",*a, *a++);
}

int* sorting(int a[],int size){
  int i,j;
  int minDiff = INT_MAX;
  int * arr = (int*)malloc(sizeof(int)*2);
  for(i=0;i<size-1;i++){
    int min = i;
    for(j = i+1; j<size; j++){
      if(a[j] < a[min]){
        min = j;
      }
    }
    if(min != i){
      int temp = a[i];
      a[i] = a[min];
      a[min] = temp;
      // printf("%d, %d\n",a[i], a[i+1] );
      // printf("Min is %d\n", abs(a[i] - a[i+1]) );
    }

   // print(a,size);

    // printf("For a[i] is %d, a[j] is %d, difference is %d\n",a[i],a[i-1], abs(a[i] - a[i-1]));
    if(abs(a[i]-a[i-1]) < minDiff){
      minDiff = abs(a[i]-a[i-1]);
      // printf("i -> %d, i+1 %d\n", a[i], a[i+1]);
      arr[0] = a[i];
      arr[1] = a[i-1];
    }

    // print(a,size);
  }
  return arr;

}

void print(int a[], int size){
  int i;
  for(i=0;i<size;i++){
    printf("%d -> ",a[i]);
  }
  printf("\n");
}
