#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>
void sorting(int[],int);
void print(int[],int);
int* findDiff(int* a, int size);
double time_elapsed(struct timespec *start, struct timespec *end);
int main(){
    int size;
    int i;
    struct timespec start;
	  struct timespec end;
    scanf("%d",&size);
    int array[size];
    for(i=0;i<size;i++){
      scanf("%d",&array[i]);
    }

	  clock_gettime(CLOCK_REALTIME, &start);
    int *a = findDiff(array,size);
    printf("Smallest Difference Pairs is %d, %d\n",a[0], a[1]);
    clock_gettime(CLOCK_REALTIME, &end);

    printf("%lf ms\n", time_elapsed(&start, &end));
    free(a);
  // int *a = sorting(array,size);
	// printf("Smallest Difference Pairs is %d, %d\n",*a, *a++);
}

void sorting(int* a,int size){
  int i,j;
  for(i=0;i<size-1;i++){
    int min = i;
    for(j = i+1; j<size; j++){
      if(a[j] < a[min]){
        min = j;
      }
    }
    if(min != i){
      int temp = a[i];
      a[i] = a[min];
      a[min] = temp;
      // printf("%d, %d\n",a[i], a[i+1] );
      // printf("Min is %d\n", abs(a[i] - a[i+1]) );
    }

  }

}

int* findDiff(int* a, int size){
  sorting(a,size);
  int* to_ret = (int*)malloc(sizeof(int)*2);
  int min_diff = INT_MAX;
  for(int i = 0; i < size-1 ; i++){
    if(abs(a[i] - a[i+1]) < min_diff){
      min_diff = abs(a[i] - a[i+1]);
      to_ret[0] = a[i];
      to_ret[1] = a[i+1];
    }
  }

  return to_ret;
}

void print(int a[], int size){
  int i;
  for(i=0;i<size;i++){
    printf("%d -> ",a[i]);
  }
  printf("\n");
}

double time_elapsed(struct timespec *start, struct timespec *end) {
	double t = 0.0;
	t = (end->tv_sec - start->tv_sec) * 1000;
	t += (end->tv_nsec - start->tv_nsec) * 0.000001;
	return t;
}
