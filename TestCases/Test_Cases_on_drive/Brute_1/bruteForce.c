#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<time.h>
int * findDiff(int * ,int );
double time_elapsed(struct timespec *start, struct timespec *end);
int main(){
  int size;
  int i;
  struct timespec start;
	struct timespec end;
  scanf("%d",&size);
  int array[size];
  for(i=0;i<size;i++){
    scanf("%d",&array[i]);
  }
  clock_gettime(CLOCK_REALTIME, &start);
  int *a = findDiff(array,size);
  clock_gettime(CLOCK_REALTIME, &end);
  printf("Smallest Difference Pairs is %d, %d\n",a[0], a[1]);
  printf("%lf ms\n", time_elapsed(&start, &end));
  free(a);
}

//TODO: figure out malloc-free
int * findDiff(int * array,int size){
  int i,j;
  int min = INT_MAX;
  int *a = (int *)malloc(sizeof(int)*2);
  for(i = 0; i<size;i++){
    for(j=i+1; j<size; j++){
      if(j == i){continue;}
      // printf("i-> %d, j -> %d\n", i,j);
      if(abs(array[i] - array[j]) < min){
        a[0] = array[i];
        a[1] = array[j];
        min = abs(array[i] - array[j]);
        // return a;
      }
    }
  }
  return a;
}

double time_elapsed(struct timespec *start, struct timespec *end) {
	double t = 0.0;
	t = (end->tv_sec - start->tv_sec) * 1000;
	t += (end->tv_nsec - start->tv_nsec) * 0.000001;
	return t;
}
