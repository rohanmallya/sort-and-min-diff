#include <stdio.h>
#include<time.h>
#include<stdlib.h>
#include<limits.h>

void sorting(int * arr, int l, int r, int n);
void merge(int *arr, int l, int r, int n);
int* findDiff(int* a, int size);
double time_elapsed(struct timespec *start, struct timespec *end);

int main()
{
	int *p;
	int n;
	
	struct timespec start, end;
	scanf("%d", &n);
	p = (int *)malloc(sizeof(int) *n);
	for (int i = 0; i<n;i++)
		scanf("%d", &p[i]);
	
	clock_gettime(CLOCK_REALTIME, &start);
	int * a = findDiff(p,n);
	clock_gettime(CLOCK_REALTIME, &end);
        printf("Smallest Difference Pairs is %d, %d\n",a[0], a[1]);
        printf("%lf ms\n", time_elapsed(&start, &end));
	free(p);
	return 0;
}

int* findDiff(int* a, int size){
  sorting(a,0,size-1,size);
  int* to_ret = (int*)malloc(sizeof(int)*2);
  int min_diff = INT_MAX;
  for(int i = 0; i < size-1 ; i++){
    if(abs(a[i] - a[i+1]) < min_diff){
      min_diff = abs(a[i] - a[i+1]);
      to_ret[0] = a[i];
      to_ret[1] = a[i+1];
    }
  }

  return to_ret;
}

void sorting(int * arr, int l, int r, int n)
{
	int mid;
	mid = (l+r)/2;
	if (l<r)
	{
		sorting(arr, l,mid,n);
		sorting(arr, mid+1, r,n);
		merge(arr, l,r,n);
	}
}

void merge(int * arr, int l, int r,int n)
{
	int * t;
	t = (int *) malloc (sizeof(int) * n);
	int i,j,k,m;
	m = (l+r)/2;
	i=l; 
	j= m+1; 
	k=l;
	while ((i<=m) && (j<=r))
	{	
		if (arr[i] < arr[j])
		{

			t[k] = arr[i];
			i+=1;
		}
		else 
		{

			t[k] = arr[j];
			j+=1;
		}
		k+=1;
	}
	
	while (i<=m)
	{
		t[k] = arr[i];
		i+=1;
		k+=1;
	}
	
	while (j<=r)
	{
		t[k] =arr[j];
		j+=1;
		k+=1;
	}
	
	for (i=l; i<=r; i++)
	{
		arr[i] = t[i];
	}
	free(t);
}

double time_elapsed(struct timespec *start, struct timespec *end) {
	double t = 0.0;
	t = (end->tv_sec - start->tv_sec) * 1000;
	t += (end->tv_nsec - start->tv_nsec) * 0.000001;
	return t;
}
