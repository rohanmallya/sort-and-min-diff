#include<stdio.h>
#include<limits.h>
#include<stdlib.h>

// Copy from a, position pos_a, a's size is size_a, to b and in b fill from pos_b
void copy(int* from_a, int from_pos_a,int size_a ,int* to_b, int from_pos_b){
	int j;
	int k = from_pos_b;
	for(j = from_pos_a; j<size_a; j++){
		to_b[k] = from_a[j];
		k++;
	}
}

void merge(int* b, int* c, int size_b, int size_c, int* to_ret){
	// int* to_ret = (int*)malloc(sizeof(int)*(size_b+size_c));
	int i = 0;
	int j = 0;
	int k = 0;
	int p = size_b;
	int q = size_c;
	while(i<p && j<q){
		if(b[i] <= c[j]){
			to_ret[k] = b[i];
			i++;
		}
		else{
			to_ret[k] = c[j];
			j++;
		}
		k++;
	}

	if(i == size_b){
		copy(c,j,size_c-1,to_ret,k);

	}else{
		//copy from b, starting from pos i,
		copy(b,i,size_b-1,to_ret,k);
	}
}

void mergesort(int* a, int size_a){

	if(size_a > 1){
		int * b = (int*)malloc(sizeof(size_a/2));
		int * c = (int*)malloc(sizeof(size_a/2));

		copy(a,0,(size_a/2)-1,b,0);
		copy(a,(size_a/2),size_a,c,0);
		mergesort(b,(size_a/2)-1);
		mergesort(c,(size_a/2)-1);
		merge(b,c,((size_a/2)-1),(((size_a/2)-1)),a);
	}
}


int main(){

	int size;
	scanf("%d",&size);
	int * array = (int*)malloc(sizeof(int)*size);
	for(int i = 0; i < size; i++){
		scanf("%d",&array[i]);
	}
	mergesort(array,size);
	for(int i = 0; i < size; i++){
		printf("%d\n",array[i]);
	}
	// copy(a,0,2,b,3);
	// for(int i = 0; i< 5; i++){
	// 	printf("%d\n",b[i]);
	// }

}
